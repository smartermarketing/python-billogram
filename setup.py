#!/usr/bin/env python
import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='billogram',
    version='0.1',
    description='Python client for billograms api, cloned from https://github.com/billogram/billogram-api-clients',
    url='https://bitbucket.org/mediamotorway/python-billogram',
    author='Billogram AB, Smarter Marketing AB',
    author_email='dev@smartermarketing.com',
    packages=['billogram'],
)
